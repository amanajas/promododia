package tarec.br.promododia.view;

import tarec.br.promododia.R;
import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.interfaces.INetworkBroadcast;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.interfaces.IPromoRequestCallback;
import tarec.br.promododia.request.PromoRequest;
import tarec.br.promododia.util.LogTrace;
import tarec.br.promododia.util.Util;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Splash extends BaseActivity implements IPromoRequestCallback, INetworkBroadcast {

	private PromoRequest mPromoRequest;

    private IntentFilter mReceiverFilter;
    private BroadcastReceiver mNetworkReceiver;

    private Button mRefreshButton;

    @Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);

		setContentView(R.layout.promo_splash);

        mReceiverFilter = new IntentFilter();
        mReceiverFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mNetworkReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

                    if (Util.isNetworkAvailable(context)) {
                        Splash.this.onNetworkConnected();
                    } else {
                        Splash.this.onNetworkDisconnected();
                    }
                }
            }
        };

        mRefreshButton = (Button) findViewById(R.id.refresh_btn);
        mRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.INVISIBLE);
                showProgress(true);
                startPromotionRequest();
            }
        });
	}

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mNetworkReceiver, mReceiverFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mNetworkReceiver);
    }
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();

        if (getActionBar() != null) {
            getActionBar().hide();
        }

		startPromotionRequest();
	}

    private void startPromotionRequest() {
		if (mPromoRequest != null) 
		{
			mPromoRequest.cancel(true);
		}

        mPromoRequest = new PromoRequest(getApplicationContext());
        mPromoRequest.execute(this);

		setLoadMessage(getResources().getString(R.string.loading_message));
		showProgress(true);
	}

	@Override
	public void onRequestFinished(PromoList list) {

        if (list == null || list != null && list.getAmount() == 0 &&
                PromoPreferences.getInstance(this).isFirstAccess()) {
            showNetworkMessage();
            return;
        }

        setLoadMessage(getResources().getString(R.string.starting_app) + getResources().getString(R.string.continuously));

        PromoPreferences.getInstance(this).setFirstAccess(false);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
	}

    @Override
    public void onStepInfo(String step) {
        setLoadMessage(step);
    }

    private void showNetworkMessage() {
        showProgress(false);
        setLoadMessage(getResources().getString(R.string.first_access_error_loading));
        mRefreshButton.setVisibility(View.VISIBLE);
    }

    @Override
	public void onException(final Exception e) {

        LogTrace.error(e);

        AlertDialog.Builder builder = Util.getAlertBuilder(this, e.getMessage());
        builder.setNeutralButton(R.string.update_promo_question_choice_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

    }

    private synchronized void setLoadMessage(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView text = (TextView)findViewById(R.id.load_text);
                text.setText(msg);
            }
        });
	}
	
	private void showProgress(final boolean value) {
		ProgressBar progress = (ProgressBar) findViewById(R.id.progressBar);
		progress.setVisibility(value ? View.VISIBLE : View.INVISIBLE);
	}

	@Override
	public void onNetworkConnected() {
        startPromotionRequest();
	}

	@Override
	public void onNetworkDisconnected() {
		if(PromoPreferences.getInstance(this).isFirstAccess()) {
            showNetworkMessage();
		}
	}
}
