package tarec.br.promododia.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import tarec.br.promododia.R;
import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.model.PromoList;

/**
 * Created by Ihgtao on 15/02/2015.
 */
public class ByCategoryAdapter extends BasePromoAdapter {

    public ByCategoryAdapter(Context context, PromoList list) {
        super(context, list);
    }

    @Override
    protected void createContent() {

        mListContent = new HashMap<String, PromoList>();
        List<Promo> promoList = mPromoList.getList();
        PromoList list;

        double limit = PromoPreferences.getInstance(getContext()).getDistanceLimit();

        for (Promo promo : promoList) {

            if (mListContent.keySet().contains(promo.getCategory())) {
                list = mListContent.get(promo.getCategory());
                list.add(promo);
            } else {
                list = new PromoList();
                list.add(promo);
                mListContent.put(promo.getCategory(), list);
            }
        }

        mListHeader = new ArrayList<String>(mListContent.keySet());

        Collections.sort(mListHeader);

    }

    @Override
    public Object getItem(int position) { return mListHeader.get(position); }

    @Override
    public int getCount() {
        return mListHeader.size();
    }

    @SuppressLint("NewApi") @Override
	public View getView(int position, View convertView, ViewGroup parent) {

        PromoViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.promo_group, null);

            holder = new PromoViewHolder();

            holder.title = (TextView) convertView.findViewById(R.id.promo_header);
            holder.desc = (TextView) convertView.findViewById(R.id.promo_top_desc);
            holder.type = PromoViewHolder.HolderType.LIST;
            convertView.setTag(holder);

        } else {

            holder = (PromoViewHolder) convertView.getTag();
        }

        final String headerTitle = (String) getItem(position);

        holder.title.setText(headerTitle.toUpperCase() + getHeaderGroupPromoAmount(headerTitle));

        holder.list = getListPromo(position);

		return convertView;
	}
}
