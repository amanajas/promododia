package tarec.br.promododia.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import tarec.br.promododia.R;

/**
 * Created by Ihgtao on 14/02/2015.
 */
public class MainMenuAdapter extends BaseAdapter {

    private WeakReference<Context> mContext;

    private int[] mMainMenuOrder;
    private int[] mMainMenuIc;

    public MainMenuAdapter(final Context context) {
        mContext = new WeakReference<Context>(context);

        mMainMenuOrder = new int[]{
                R.string.title_section1,
                R.string.title_section2,
                R.string.title_section3,
                R.string.title_section4,
                R.string.action_settings
        };

        mMainMenuIc = new int[] {
                R.drawable.ic_menu_today,
                R.drawable.ic_menu_week,
                R.drawable.ic_menu_day,
                R.drawable.ic_menu_agenda,
                R.drawable.ic_menu_preferences
        };

    }

    @Override
    public int getCount() {
        return mMainMenuOrder.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {

            LayoutInflater inflater = ((Activity) mContext.get()).getLayoutInflater();
            convertView = inflater.inflate(R.layout.menu_item, parent, false);

            holder = new ViewHolder();

            holder.text = (TextView) convertView;

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(mMainMenuOrder[position]);
        holder.text.setCompoundDrawablesWithIntrinsicBounds(mMainMenuIc[position], 0, 0, 0);

        return convertView;
    }

    private static class ViewHolder {

        TextView text;
    }
}
