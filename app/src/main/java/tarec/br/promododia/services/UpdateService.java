package tarec.br.promododia.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.Timer;
import java.util.TimerTask;

import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.interfaces.IPromoRequestCallback;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.request.PromoRequest;
import tarec.br.promododia.util.LogTrace;
import tarec.br.promododia.util.Util;

/**
 * Created by Ihgtao on 14/03/2015.
 */
public class UpdateService extends Service implements IPromoRequestCallback{
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        LogTrace.debug("Update service started.");

        scheduleUpdate();

        return START_STICKY;
    }

    private void scheduleUpdate() {
        Timer time = new Timer();
        ScheduledTask st = new ScheduledTask();
        time.schedule(st, PromoPreferences.getInstance(this).getUpdateAccess());

        LogTrace.debug("Update service to: " + Util.getDateToString(this,
                PromoPreferences.getInstance(this).getUpdateAccess()));
    }

    @Override
    public void onRequestFinished(PromoList list) {
        scheduleUpdate();
    }

    @Override
    public void onStepInfo(String step) {
        LogTrace.debug(step);
    }

    @Override
    public void onException(Exception e) {
        LogTrace.error(e);
    }

    public class ScheduledTask extends TimerTask {

        @Override
        public void run() {
            PromoRequest request = new PromoRequest(UpdateService.this.getApplicationContext());
            request.execute(UpdateService.this);
        }
    }
}
