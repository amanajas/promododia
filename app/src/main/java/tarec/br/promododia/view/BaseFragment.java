package tarec.br.promododia.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ListView;

import tarec.br.promododia.R;
import tarec.br.promododia.adapters.BasePromoAdapter;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Constants;
import tarec.br.promododia.util.LogTrace;

/**
 * Created by Ihgtao on 17/02/2015.
 */
public class BaseFragment extends Fragment {

    protected BasePromoAdapter mAdapter;
    protected ListView mListView;

    public BaseFragment() {
        mAdapter = null;
    }

    protected void setBackButtonVisible(final View view, final boolean b) {
        View backButton = view.findViewById(R.id.linear_view_click);
        if (b) {
            backButton.setVisibility(View.VISIBLE);
        } else {
            backButton.setVisibility(View.GONE);
        }
    }

    protected final void showEmptyList(final View view, final boolean b) {
        View emptyList = view.findViewById(R.id.empty_list);
        if (b) {
            emptyList.setVisibility(View.VISIBLE);
        } else {
            emptyList.setVisibility(View.GONE);
        }
    }
}
