package tarec.br.promododia.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import tarec.br.promododia.R;
import tarec.br.promododia.adapters.BasePromoAdapter;
import tarec.br.promododia.adapters.ShowPromotionAdapter;
import tarec.br.promododia.interfaces.OnFragmentInteractionListener;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Constants;
import tarec.br.promododia.util.Util;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link tarec.br.promododia.interfaces.OnFragmentInteractionListener}
 * interface.
 */
public class ShowPromotionFragment extends BaseFragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private OnFragmentInteractionListener mListener;

    private PromoList mPromoList;
    private String mTitle;
    private String mDescription;

    public static Fragment newInstance() {
        ShowPromotionFragment fragment = new ShowPromotionFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ShowPromotionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promotions, container, false);

        if (savedInstanceState != null) {
            mPromoList = (PromoList) savedInstanceState.getSerializable(Constants.KEY_PROMO_LIST);
            mTitle = savedInstanceState.getString(Constants.KEY_TITLE_LIST, this.getActivity().
                    getResources().getString(R.string.none));
            mDescription = savedInstanceState.getString(Constants.KEY_DESC_LIST, this.getActivity().
                    getResources().getString(R.string.none));
        } else {
            mPromoList = (PromoList) getArguments().getSerializable(Constants.KEY_PROMO_LIST);
            mTitle = getArguments().getString(Constants.KEY_TITLE_LIST, this.getActivity().
                    getResources().getString(R.string.none));
            mDescription = getArguments().getString(Constants.KEY_DESC_LIST, this.getActivity().
                    getResources().getString(R.string.none));
        }

        setBackButtonVisible(view, true);

        mAdapter = new ShowPromotionAdapter(this.getActivity(), mPromoList);

        // Get list view
        mListView = (ListView) view.findViewById(android.R.id.list);

        // Set header
        View header = view.findViewById(R.id.linear_view_click);
        header.setTag(new BasePromoAdapter.PromoViewHolder());
        header.setOnClickListener(this);
        TextView title = (TextView) header.findViewById(R.id.promo_header);
        TextView desc = (TextView) header.findViewById(R.id.promo_top_desc);
        title.setText(mTitle);
        desc.setText(mDescription);

        // Set adapter
        mListView.setAdapter(mAdapter);

        //Show empty list
        showEmptyList(view, mPromoList.getAmount() == 0);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putSerializable(Constants.KEY_PROMO_LIST, mPromoList);
        outState.putString(Constants.KEY_TITLE_LIST, mTitle);
        outState.putString(Constants.KEY_DESC_LIST, mDescription);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction(view);

        }
    }

    @Override
    public void onClick(View v) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction(v);

        }
    }
}
