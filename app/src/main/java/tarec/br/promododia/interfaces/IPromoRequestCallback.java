package tarec.br.promododia.interfaces;


import tarec.br.promododia.model.PromoList;

public interface IPromoRequestCallback {

	void onRequestFinished(PromoList list);
    void onStepInfo(String step);
	void onException(Exception e);
}
