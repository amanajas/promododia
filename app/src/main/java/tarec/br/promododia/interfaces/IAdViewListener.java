package tarec.br.promododia.interfaces;

import android.content.Context;

/**
 * Created by Ihgtao on 19/02/2015.
 */
public interface IAdViewListener {

    void onLoaded();
    void onClosed();
    Context getContext();
}
