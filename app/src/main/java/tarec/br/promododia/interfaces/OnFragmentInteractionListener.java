package tarec.br.promododia.interfaces;

import android.view.View;

/**
 * Created by Ihgtao on 14/02/2015.
 */
public interface OnFragmentInteractionListener {

    public void onFragmentInteraction(View view);
}
