package tarec.br.promododia.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import tarec.br.promododia.services.UpdateService;

/**
 * Created by Ihgtao on 14/03/2015.
 */
public class BootAndUpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") ||
                intent.getAction().equals("android.intent.action.MY_PACKAGE_REPLACED"))
        {
            Intent startServiceIntent = new Intent(context, UpdateService.class);
            context.startService(startServiceIntent);
        }
    }
}
