package tarec.br.promododia.interfaces;

public interface INetworkBroadcast {

	void onNetworkConnected();
	void onNetworkDisconnected();
}
