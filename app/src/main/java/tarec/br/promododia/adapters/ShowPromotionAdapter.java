package tarec.br.promododia.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import tarec.br.promododia.R;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.model.PromoList;

/**
 * Created by Ihgtao on 15/02/2015.
 */
public class ShowPromotionAdapter extends BasePromoAdapter {
    public ShowPromotionAdapter(Context context, PromoList list) {
        super(context, list);
    }

    @Override
    public Object getItem(int position) { return mPromoList.getIndex(position); }

    @Override
    public int getCount() { return mPromoList.getAmount(); }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PromoViewHolder holder;

		if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.promo_row, null);

            holder = new PromoViewHolder();

            holder.title = (TextView) convertView.findViewById(R.id.promo_title);
            holder.desc = (TextView) convertView.findViewById(R.id.promo_days);
            holder.star = (ImageView) convertView.findViewById(R.id.premium_flag);
            holder.type = PromoViewHolder.HolderType.PROMO;

            convertView.setTag(holder);

        } else {

            holder = (PromoViewHolder) convertView.getTag();
        }

        Promo promo = (Promo) getItem(position);

		holder.title.setText(promo.getTitle());
        holder.desc.setText(promo.getPeriodicity());
        holder.promotion = promo;
        if (promo.isPremium()) {
            holder.star.setImageResource(android.R.drawable.btn_star_big_on);
        } else {
            holder.star.setImageResource(R.drawable.ic_menu_info_details);
        }

		return convertView;
	}
}
