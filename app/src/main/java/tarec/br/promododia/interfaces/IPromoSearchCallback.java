package tarec.br.promododia.interfaces;

import tarec.br.promododia.model.PromoList;

/**
 * Created by Ihgtao on 19/02/2015.
 */
public interface IPromoSearchCallback {

    void onSearchFinished(PromoList list);
    void onSearchError(Exception e);
}
