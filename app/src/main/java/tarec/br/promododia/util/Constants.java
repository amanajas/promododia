package tarec.br.promododia.util;

public class Constants {

	public static final String EXTRA_PROMO = "extra_promo";

    public static final String KEY_PROMO_LIST = "kpromol";
    public static final String KEY_TITLE_LIST = "ktitlel";
    public static final String KEY_DESC_LIST = "kdescl";

    public static final String PROMO_DATABASE = "promo_db";

    public static final String BROADCAST_GPS_UPDATED = "tarec.br.promododia.GPS_UPDATED";

    public static final String WEB_URL = "www.promododia.com";
}
