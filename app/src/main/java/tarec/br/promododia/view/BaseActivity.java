package tarec.br.promododia.view;

import tarec.br.promododia.R;
import tarec.br.promododia.util.Util;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.Window;

public class BaseActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
        this.overridePendingTransition(R.anim.open_in, R.anim.open_out);
	}

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        this.overridePendingTransition(R.anim.close_in, R.anim.close_out);
    }

    protected final void showExitOption() {
        AlertDialog.Builder builder = Util.getAlertBuilder(this, R.string.exit_message);
        builder.setPositiveButton(R.string.exit_message_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                BaseActivity.this.finish();
            }
        });
        builder.setNegativeButton(R.string.exit_message_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
