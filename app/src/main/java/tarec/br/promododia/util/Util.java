package tarec.br.promododia.util;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.View;

import org.apache.http.ParseException;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import tarec.br.promododia.R;
import tarec.br.promododia.services.UpdateService;

public class Util {

    private static final DecimalFormat KM_FORMATTER = new DecimalFormat("#.#");
    private static final String SEARCH_GEO_LOCATION = "geo:0,0?q=";

	public static boolean isNetworkAvailable(final Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected()
				&& activeNetworkInfo.isAvailable();
	}

	public static Date getDateFromString(final Context context, final String dateStr) {
		Date date = new Date(System.currentTimeMillis());
		try {

            final String dateFormat = context.getResources().getString(R.string.date_format);
			final SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
			date = (Date) format.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static String getDateToString(final Context context, final Date date) {

        final String dateFormat = context.getResources().getString(R.string.date_format);

		SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
		return format.format(date);
	}

    public static boolean checkService(final Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (UpdateService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static double getDistanceByGeocode(final double lat1, final double lon1, final Double lat2, final Double lon2) {

        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) *
                Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;

        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public static String formatDistance(final Context context, final double distance) {

        final String distStr = distance <= 0 ? context.getResources().getString(R.string.none) :
                KM_FORMATTER.format(distance);

        final String km = distStr.length() > 0 ? distStr + " " +
                context.getResources().getString(R.string.distance_desc) : distStr;

        return km;
    }

    public static AlertDialog.Builder getAlertBuilder(final Context context, final int message) {
        return getAlertBuilder(context, context.getResources().getString(message));
    }

    public static AlertDialog.Builder getAlertBuilder(final Context context, final String message) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setMessage(message);
        alertBuilder.setTitle(R.string.app_name);
        alertBuilder.setCancelable(false);
        alertBuilder.setIcon(R.drawable.ic_launcher);
        return alertBuilder;
    }
}
