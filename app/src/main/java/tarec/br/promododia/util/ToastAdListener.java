package tarec.br.promododia.util;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;

import tarec.br.promododia.interfaces.IAdViewListener;

/**
 * Created by Ihgtao on 19/02/2015.
 */
public class ToastAdListener extends AdListener {
    private IAdViewListener mContext;

    public ToastAdListener(IAdViewListener context) {
        this.mContext = context;
    }

    @Override
    public void onAdLoaded() {
        mContext.onLoaded();
    }

    @Override
    public void onAdFailedToLoad(int errorCode) {
        String errorReason = "";
        switch(errorCode) {
            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                errorReason = "Internal error";
                break;
            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                errorReason = "Invalid request";
                break;
            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                errorReason = "Network Error";
                break;
            case AdRequest.ERROR_CODE_NO_FILL:
                errorReason = "No fill";
                break;
        }
        LogTrace.error(errorReason);
    }

    @Override
    public void onAdOpened() {
        LogTrace.debug("Ad opened.");
    }

    @Override
    public void onAdClosed() {
        mContext.onClosed();
    }

    @Override
    public void onAdLeftApplication() {
        LogTrace.debug("Ad left application.");
    }
}