package tarec.br.promododia.config;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;
import java.util.Date;

import tarec.br.promododia.R;

public class PromoPreferences {

    private static final String CURRENT_GPS_LOCATION_LAT = "cloclat";
    private static final String CURRENT_GPS_LOCATION_LNG = "cloclng";
    private static final String CURRENT_GPS_DEFAULT_VALUE = "0";

    private static final String PROMO_SHARED_NAME = "PROMO_PREFERENCES";

    private static final String PROMO_FIRST_ACCESS = "firstaccess";
    private static final String PROMO_UPDATE_ACCESS = "updateaccess";

    private static final String PROMO_DISTANCE_LIMIT = "distancelimit";
    private static final int DEFAULT_DISTANCE = 10;
    private static final String PROMO_DISTANCE_LIMIT_PROGRESS = "distanceprogressbar";
    private static final int DEFAULT_DISTANCE_PROGRESS = 33;

    private static PromoPreferences mInstance;

	private SharedPreferences mShared;
	private SharedPreferences.Editor mEditor;

	private PromoPreferences(final Context context) {
		mShared = context.getSharedPreferences(PROMO_SHARED_NAME, Activity.MODE_PRIVATE);
		mEditor = mShared.edit();
	}
	
	public static PromoPreferences getInstance(final Context context) {
		if (mInstance == null) {
			mInstance = new PromoPreferences(context);
		}
		return mInstance;
	}

    public void setFirstAccess(final boolean value) {
        mEditor.putBoolean(PROMO_FIRST_ACCESS, value);
        mEditor.commit();
    }

    public void renewUpdateAccess() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date value = calendar.getTime();
        mEditor.putLong(PROMO_UPDATE_ACCESS, value.getTime());
        mEditor.commit();
    }

    public Date getUpdateAccess() {
        final long millis = mShared.getLong(PROMO_UPDATE_ACCESS, 0);
        return millis != 0 ? new Date(millis) : null;
    }
	
	public boolean isFirstAccess() {
		return mShared.getBoolean(PROMO_FIRST_ACCESS, true);
	}

    public void setLastLocation(final double lat, final double lng) {
        mEditor.putString(CURRENT_GPS_LOCATION_LAT, String.valueOf(lat));
        mEditor.putString(CURRENT_GPS_LOCATION_LNG, String.valueOf(lng));
        mEditor.commit();
    }

    public double[] getLastLocation() {
        String lat = mShared.getString(CURRENT_GPS_LOCATION_LAT, CURRENT_GPS_DEFAULT_VALUE);
        String lng = mShared.getString(CURRENT_GPS_LOCATION_LNG, CURRENT_GPS_DEFAULT_VALUE);

        return new double[]{Double.valueOf(lat), Double.valueOf(lng)};
    }

    public void setDistanceLimit(final int value, final int progressBar) {
        mEditor.putInt(PROMO_DISTANCE_LIMIT, value);
        mEditor.putInt(PROMO_DISTANCE_LIMIT_PROGRESS, progressBar);
        mEditor.commit();
    }

    public String getDistanceLimit(final Context context) {
        final String km = context.getResources().getString(R.string.distance_desc);
        return mShared.getInt(PROMO_DISTANCE_LIMIT, DEFAULT_DISTANCE) + km;
    }

    public double getDistanceLimit() {
        return mShared.getInt(PROMO_DISTANCE_LIMIT, DEFAULT_DISTANCE);
    }

    public int getDistanceProgressBar() {
        return mShared.getInt(PROMO_DISTANCE_LIMIT_PROGRESS, DEFAULT_DISTANCE_PROGRESS);
    }
}
