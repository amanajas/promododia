package tarec.br.promododia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import tarec.br.promododia.dao.Promo;


public class PromoList implements Serializable {

	private static final long serialVersionUID = 2180098429428496415L;

	private List<Promo> mList;
	
	public PromoList() {
		this(null);
	}
	
	public PromoList(final List<Promo> list) {

        mList = list != null ? list : new ArrayList<Promo>();
	}
	
	public void add(final Promo promo) {
		mList.add(promo);
	}
	
	public int getAmount() {
		return mList.size();
	}
	
	public Promo getIndex(final int index) {
		return mList.get(index);
	}
	
	public List<Promo> getList() {
		return new ArrayList<Promo>(mList);
	}

	public void removePromos(List<Promo> promoToRemove) {
		mList.removeAll(promoToRemove);		
	}
	
	public void removePromo(Promo promo) {
		mList.remove(promo);
	}
	
	public void addPromos(List<Promo> promoToAdd) {
		mList.addAll(promoToAdd);
	}

    public void setList(List<Promo> promos) {
        mList = new ArrayList<>(promos);
    }

    public void clear() { mList.clear(); }

    public void addList(PromoList list) {
        mList.addAll(list.getList());
    }
}
