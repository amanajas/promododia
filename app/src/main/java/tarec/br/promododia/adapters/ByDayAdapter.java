package tarec.br.promododia.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import tarec.br.promododia.R;
import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Util;

/**
 * Created by Ihgtao on 15/02/2015.
 */
public class ByDayAdapter extends BasePromoAdapter {

    private String mToday;

    public ByDayAdapter(Context context, PromoList list) {
        super(context, list);
    }

    @Override
    protected void createContent() {

        mListContent = new HashMap<String, PromoList>();
        List<Promo> promoList = mPromoList.getList();
        PromoList list;

        String[] weekDays = new DateFormatSymbols(new Locale(getContext().getString(R.string.language),
                getContext().getString(R.string.country))).getWeekdays();

        Calendar calendar = Calendar.getInstance();
        mToday = weekDays[calendar.get(Calendar.DAY_OF_WEEK)].toUpperCase();

        for (String day : weekDays) {

            if (day.length() > 0) {

                for (Promo promo : promoList) {

                    if (promo.getPeriodicity().toUpperCase().contains(day.substring(0,3).toUpperCase())) {

                        if (mListContent.keySet().contains(day)) {
                            list = mListContent.get(day);
                            list.add(promo);
                        } else {
                            list = new PromoList();
                            list.add(promo);
                            mListContent.put(day, list);
                        }
                    }
                }
            }
        }

        // Order by day
        String[] weekDayOrder = getContext().getResources().getStringArray(R.array.week_days_order);
        List<String> tempList = new ArrayList<String>(mListContent.keySet());
        mListHeader = new ArrayList<String>();

        for (String order : weekDayOrder) {
            for (String day : tempList) {
                if (day.contains(order)) {
                    mListHeader.add(day);
                    break;
                }
            }
        }

    }

    @Override
    public Object getItem(int position) { return mListHeader.get(position); }

    @Override
    public int getCount() {
        return mListHeader.size();
    }

    @SuppressLint("NewApi") @Override
	public View getView(int position, View convertView, ViewGroup parent) {

        PromoViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.promo_group, null);

            holder = new PromoViewHolder();

            holder.title = (TextView) convertView.findViewById(R.id.promo_header);
            holder.desc = (TextView) convertView.findViewById(R.id.promo_top_desc);
            holder.type = PromoViewHolder.HolderType.LIST;
            convertView.setTag(holder);

        } else {

            holder = (PromoViewHolder) convertView.getTag();
        }

        final String headerTitle = (String) getItem(position);

        String todayFlag = "";
        if (headerTitle.equalsIgnoreCase(mToday)) {
            todayFlag = this.getContext().getResources().getString(R.string.promo_day_desc);
        }

        holder.title.setText(headerTitle.toUpperCase() + getHeaderGroupPromoAmount(headerTitle));
        holder.desc.setText(todayFlag);
        holder.list = getListPromo(position);

		return convertView;
	}
}
