package tarec.br.promododia.request;

import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.List;

import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.dao.PromoDatabase;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.interfaces.IPromoSearchCallback;

/**
 * Created by Ihgtao on 19/02/2015.
 */
public class PromoSearch extends AsyncTask<IPromoSearchCallback, Void, PromoList> {

    private IPromoSearchCallback[] mCallback;
    private WeakReference<Context> mContext;
    private String mSearchQuery;

    public PromoSearch(final Context context, final String query) {
        mContext = new WeakReference<Context>(context);
        mSearchQuery = query;
    }

    @Override
    protected PromoList doInBackground(IPromoSearchCallback... params) {

        mCallback = params;

        PromoList result = new PromoList();

        try {
            PromoList promos = PromoDatabase.getInstance(mContext.get()).getAllPromotions();
            List<Promo> list = promos.getList();
            for (Promo promo : list) {
                if (promo.getDescription().toLowerCase().contains(mSearchQuery.toLowerCase()) ||
                        promo.getLabel().toLowerCase().contains(mSearchQuery.toLowerCase())) {
                    result.add(promo);
                }
            }

        }catch (Exception e) {
            sendException(e);
        }

        return result;
    }

    @Override
    protected void onPostExecute(PromoList list) {
        super.onPostExecute(list);
        for (IPromoSearchCallback callback : mCallback) {
            callback.onSearchFinished(list);
        }
    }

    private void sendException(Exception e) {
        for (IPromoSearchCallback callback : mCallback) {
            callback.onSearchError(e);
        }
    }
}
