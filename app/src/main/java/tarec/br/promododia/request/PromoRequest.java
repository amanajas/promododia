package tarec.br.promododia.request;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import tarec.br.promododia.R;
import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.dao.PromoDatabase;
import tarec.br.promododia.interfaces.IPromoRequestCallback;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Constants;
import tarec.br.promododia.util.LogTrace;
import tarec.br.promododia.util.Util;

public class PromoRequest extends
        AsyncTask<IPromoRequestCallback, Void, PromoList> implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int DEFAULT_CONNECTION_TIMEOUT = 10000;
    private static final int HTTP_CONNECTION_TIMEOUT = 50000;

    private static final String PROMO_URL = "http://www.promododia.com/feed";

    private static final String TAG_ITEM = "item";
    private static final String TAG_TITLE = "title";
    private static final String TAG_DESC = "description";
    private static final String TAG_GU_ID = "link";
    private static final String TAG_IMAGE_URL = "thumb";
    private static final String TAG_IMAGE_LABEL = "label";
    private static final String TAG_START_DATE = "start_date";
    private static final String TAG_END_DATE = "end_date";
    private static final String TAG_PERIODICITY = "periodicity";
    private static final String TAG_CITY = "city";
    private static final String TAG_CATEGORY = "category";
    private static final String TAG_ADDRESS = "address";
    private static final String TAG_CODE = "code";
    private static final String TAG_LATITUDE = "lat";
    private static final String TAG_LONGITUDE = "lng";
    private static final String TAG_PREMIUM = "premium";

    private static final String PASS_POST = "7K,:U/JR,E/CNF3*As"; // 7K,:U/j-sSz+B,E/CNF3*As // 7K,:U/j-sSz+B,E/CNF // "*fth+874+68/73-/3";


    private static final String NAME_POST = "dogma_";


    private WeakReference<Context> mContext;
	private IPromoRequestCallback[] mCallbackList;
    private boolean mForceExecution = false;

    private GoogleApiClient mGoogleApiClient;

    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;

    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError;
    private PromoList mRequestResult;

    public PromoRequest(Context context) {
        mContext = new WeakReference<Context>(context);
        buildGoogleApiClient();
    }

    private synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(mContext.get())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mResolvingError = false;
    }

	@Override
	protected PromoList doInBackground(IPromoRequestCallback... arg0) {

        PromoList list = PromoDatabase.getInstance(mContext.get()).getAllPromotions();

        Date nextUpdate = PromoPreferences.getInstance(mContext.get()).getUpdateAccess();

        sendStepInfo(mContext.get().getResources().getString(R.string.promo_request_step1));

        mCallbackList = arg0;

        if (mForceExecution || list.getList().size() == 0 || nextUpdate == null ||
                nextUpdate != null && nextUpdate.before(new Date())) {

            list = new PromoList();

            this.mForceExecution = false;

            sendStepInfo(mContext.get().getResources().getString(R.string.promo_request_step2));

            if (Util.isNetworkAvailable(mContext.get())) {

                enableHttpResponseCache();
                disableConnectionReuseIfNecessary();

                try {
                    XmlPullParserFactory factory = XmlPullParserFactory
                            .newInstance();
                    factory.setNamespaceAware(false);
                    XmlPullParser xpp = factory.newPullParser();

                    HttpParams httpParams = new BasicHttpParams();

                    ConnManagerParams.setTimeout(httpParams, DEFAULT_CONNECTION_TIMEOUT);
                    HttpConnectionParams.setConnectionTimeout(httpParams, DEFAULT_CONNECTION_TIMEOUT);
                    HttpConnectionParams.setSoTimeout(httpParams, HTTP_CONNECTION_TIMEOUT);

                    HttpClient httpClient = new DefaultHttpClient(httpParams);
                    HttpPost httpPost = new HttpPost(PROMO_URL);
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                    nameValuePairs.add(new BasicNameValuePair(NAME_POST, PASS_POST));
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    HttpEntity httpEntity = httpResponse.getEntity();

                    if (httpEntity != null) {
                        InputStream inputStream = httpEntity.getContent();

                        if (inputStream != null) {
                            xpp.setInput(inputStream, HTTP.ISO_8859_1);

                            boolean insideItem = false;

                            sendStepInfo(mContext.get().getResources().getString(R.string.promo_request_step3));

                            int eventType = xpp.getEventType();
                            Promo promoTemp = null;
                            while (eventType != XmlPullParser.END_DOCUMENT) {
                                if (eventType == XmlPullParser.START_TAG) {

                                    if (!insideItem
                                            && xpp.getName().equalsIgnoreCase(
                                            TAG_ITEM)) {
                                        insideItem = true;
                                        promoTemp = new Promo();
                                    } else if (insideItem) {
                                        if (xpp.getName().equalsIgnoreCase(
                                                TAG_CODE)) {
                                            promoTemp.setCode(Integer.valueOf(xpp.nextText()));
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_TITLE)) {
                                            promoTemp.setTitle(xpp.nextText());
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_IMAGE_LABEL)) {
                                            promoTemp.setLabel(xpp.nextText());
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_DESC)) {
                                            promoTemp
                                                    .setDescription(xpp.nextText());
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_GU_ID)) {
                                            promoTemp.setLink(xpp.nextText());
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_IMAGE_URL)) {
                                            promoTemp.setImage(xpp.nextText());
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_CATEGORY)) {
                                            promoTemp.setCategory(xpp.nextText());
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_CITY)) {
                                            promoTemp.setCity(xpp.nextText());
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_END_DATE)) {
                                            promoTemp.setEnd(Util
                                                    .getDateFromString(mContext.get(), xpp
                                                            .nextText()));
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_PERIODICITY)) {
                                            promoTemp
                                                    .setPeriodicity(xpp.nextText());
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_START_DATE)) {
                                            promoTemp.setStart(Util
                                                    .getDateFromString(mContext.get(), xpp
                                                            .nextText()));
                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_ADDRESS)) {
                                            promoTemp.setAddress(xpp.nextText());

                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_LATITUDE)) {
                                            promoTemp.setLatitude(Double.valueOf(xpp.nextText()));

                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_LONGITUDE)) {
                                            promoTemp.setLongitude(Double.valueOf(xpp.nextText()));

                                        } else if (xpp.getName().equalsIgnoreCase(
                                                TAG_PREMIUM)) {
                                            promoTemp.setPremium(Integer.valueOf(xpp.nextText()));
                                        }
                                    }
                                } else if (eventType == XmlPullParser.END_TAG
                                        && xpp.getName().equalsIgnoreCase(
                                        TAG_ITEM)) {
                                    insideItem = false;
                                    list.add(promoTemp);
                                }

                                eventType = xpp.next();
                            }
                        }
                    }

                } catch (ConnectTimeoutException e) {
                    sendException(e);
                } catch (SocketTimeoutException e) {
                    sendException(e);
                } catch (MalformedURLException e) {
                    sendException(e);
                } catch (XmlPullParserException e) {
                    sendException(e);
                } catch(NumberFormatException e) {
                    sendException(e);
                } catch (IOException e) {
                    sendException(e);
                }
            }
        }

		return list;
	}

    private void enableHttpResponseCache() {
        try {
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            File httpCacheDir = new File(mContext.get().getCacheDir(), "http");
            Class.forName("android.net.http.HttpResponseCache")
                    .getMethod("install", File.class, long.class)
                    .invoke(null, httpCacheDir, httpCacheSize);
        } catch (Exception httpResponseCacheNotAvailable) {
            LogTrace.error(httpResponseCacheNotAvailable);
        }
    }

    private void disableConnectionReuseIfNecessary() {
        // HTTP connection reuse which was buggy pre-froyo
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }
    }

	@Override
	protected void onPostExecute(PromoList result) {
		super.onPostExecute(result);

		if (result != null) {
            mRequestResult = result;

            refreshStatus(mRequestResult);

            sendStepInfo(mContext.get().getResources().getString(R.string.promo_request_step4));

            setAlternativeCurrentLocationFromProvider();

            mGoogleApiClient.connect();

            sendRequestFinished();
		}
	}

    private void setAlternativeCurrentLocationFromProvider() {
        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) mContext.get().getSystemService(Context.LOCATION_SERVICE);

        String gpsProvider = LocationManager.GPS_PROVIDER;
        if (Util.isNetworkAvailable(mContext.get())) {
            gpsProvider = LocationManager.NETWORK_PROVIDER;
        }

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                PromoPreferences.getInstance(mContext.get()).setLastLocation(location.getLatitude(),
                        location.getLongitude());
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(gpsProvider, 0, 0, locationListener);
    }

    protected void sendException(Exception e) {
		if (mCallbackList != null) {
			for (IPromoRequestCallback callback : mCallbackList) {
				callback.onException(e);
			}
		}
	}

    protected void sendRequestFinished() {
        if (mCallbackList != null) {
            for (IPromoRequestCallback callback : mCallbackList) {
                callback.onRequestFinished(mRequestResult);
            }
        }
    }

    protected void sendStepInfo(String step) {
        if (mCallbackList != null) {
            for (IPromoRequestCallback callback : mCallbackList) {
                callback.onStepInfo(step);
            }
        }
    }

    protected void sendFalseResult() {
        if (mCallbackList != null) {
            for (IPromoRequestCallback callback : mCallbackList) {
                callback.onRequestFinished(null);
            }
        }
    }

    public void forceExecution(IPromoRequestCallback... arg0) {

        this.mForceExecution = true;
        this.execute(arg0);
    }

    @Override
    public void onConnected(Bundle bundle) {

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                try {

                    LogTrace.debug("Google API ok.");

                    Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(
                            mGoogleApiClient);

                    PromoPreferences.getInstance(mContext.get()).setLastLocation(currentLocation.getLatitude(),
                            currentLocation.getLongitude());

                    List<Promo> list = mRequestResult.getList();
                    final double[] location = PromoPreferences.getInstance(mContext.get()).getLastLocation();

                    Collections.sort(list, new Comparator<Promo>() {
                        @Override
                        public int compare(Promo lhs, Promo rhs) {

                            double dist1 = lhs.getDistance();
                            if (dist1 == 0)
                                dist1 = Util.getDistanceByGeocode(location[0], location[1], lhs.getLatitude(), lhs.getLongitude());

                            double dist2 = rhs.getDistance();
                            if(dist2 == 0)
                                dist2 = Util.getDistanceByGeocode(location[0], location[1], rhs.getLatitude(), rhs.getLongitude());

                            lhs.setDistance(dist1);
                            rhs.setDistance(dist2);

                            return dist1 < dist2 ? -1 : (dist1 > dist2 ? 1 : 0);
                        }
                    });
                    mRequestResult = new PromoList(list);

                    refreshStatus(mRequestResult);

                    mGoogleApiClient.disconnect();

                    LocalBroadcastManager.getInstance(mContext.get()).sendBroadcast(new Intent(Constants.BROADCAST_GPS_UPDATED));

                } catch (Exception e) {

                    LogTrace.error(e);
                    sendException(e);
                }
            }
        });
    }

    private void refreshStatus(final PromoList list) {
        PromoDatabase.getInstance(mContext.get()).renewPromo(list);
        PromoPreferences.getInstance(mContext.get()).renewUpdateAccess();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        LogTrace.error(connectionResult.toString());

        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult((android.app.Activity) mContext.get(), REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            LogTrace.error(String.valueOf(connectionResult.getErrorCode()));
            mResolvingError = true;
            sendFalseResult();
        }
    }
}
