package tarec.br.promododia.dao;

import de.greenrobot.dao.query.QueryBuilder;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Constants;

import android.content.Context;

import java.util.Date;

public class PromoDatabase {

    private static PromoDatabase mInstance = null;

    private DaoMaster.DevOpenHelper mHelper;

    private PromoDatabase(final Context context) {
        mHelper = new DaoMaster.DevOpenHelper(context,
                Constants.PROMO_DATABASE, null);
    }

    public static PromoDatabase getInstance(final Context context) {
        if (mInstance == null) {
            mInstance = new PromoDatabase(context);
        }
        return mInstance;
    }

    private DaoSession getSession() {
        DaoMaster daoMaster = new DaoMaster(mHelper.getWritableDatabase());
        return daoMaster.newSession();
    }

    public PromoList getAllPromotions() {
        DaoSession session = getSession();
        PromoDao dao = session.getPromoDao();
        QueryBuilder<Promo> query = dao.queryBuilder();
        query.where(PromoDao.Properties.End.ge(new Date()));
        query.orderAsc(PromoDao.Properties.End);
        session.clear();
        return new PromoList(query.list());
    }

    private PromoList getUpdatedList(final double distance, final int premium) {
        DaoSession session = getSession();
        PromoDao dao = session.getPromoDao();
        QueryBuilder builder = dao.queryBuilder();
        builder.where(
                PromoDao.Properties.Distance.le(distance),
                PromoDao.Properties.End.ge(new Date()),
                PromoDao.Properties.Premium.eq(premium)
        );
        builder.orderAsc(PromoDao.Properties.Distance);
        PromoList list = new PromoList(builder.list());
        session.clear();
        return list;
    }

    public PromoList getSearchByDistance(final double distance) {

        PromoList listWithOutPremium = getUpdatedList(distance, 0);
        PromoList premiumList = getUpdatedList(distance, 1);
        premiumList.addList(listWithOutPremium);

        return premiumList;
    }

    public void renewPromo(PromoList entities) {
        DaoSession session = getSession();
        PromoDao dao = session.getPromoDao();
        dao.insertOrReplaceInTx(entities.getList());
        session.clear();
    }
}