package tarec.br.promododia.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tarec.br.promododia.R;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Util;

/**
 * Created by Ihgtao on 15/02/2015.
 */
public class ByLocationAdapter extends BasePromoAdapter {

    public ByLocationAdapter(Context context, PromoList list) {
        super(context, list);
    }

    protected void createContent() {

        mListContent = new HashMap<String, PromoList>();
        mListHeader = new ArrayList<String>();
        List<Promo> promoList = mPromoList.getList();
        PromoList list;

        for (Promo promo : promoList) {

            if (mListContent.keySet().contains(promo.getLabel())) {
                list = mListContent.get(promo.getLabel());
                list.add(promo);
            } else {
                list = new PromoList();
                list.add(promo);
                mListHeader.add(promo.getLabel());
                mListContent.put(promo.getLabel(), list);
            }
        }
    }

    @Override
    public Object getItem(int position) { return mListHeader.get(position); }

    @Override
    public int getCount() {
        return mListHeader.size();
    }

    @SuppressLint("NewApi") @Override
	public View getView(int position, View convertView, ViewGroup parent) {

        PromoViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.promo_group, null);

            holder = new PromoViewHolder();

            holder.title = (TextView) convertView.findViewById(R.id.promo_header);
            holder.desc = (TextView) convertView.findViewById(R.id.promo_top_desc);
            holder.star = (ImageView) convertView.findViewById(R.id.premium_flag);
            holder.type = PromoViewHolder.HolderType.LIST;

            convertView.setTag(holder);

        } else {

            holder = (PromoViewHolder) convertView.getTag();
        }

        final String headerTitle = (String) getItem(position);

        Promo promo = getListPromo(headerTitle).getIndex(0);

        holder.title.setText(headerTitle + getHeaderGroupPromoAmount(headerTitle));
        holder.desc.setText(Util.formatDistance(getContext(), promo.getDistance()));
        holder.list = getListPromo(headerTitle);
        if (promo.isPremium()) {
            holder.star.setImageResource(android.R.drawable.btn_star_big_on);
        } else {
            holder.star.setImageResource(R.drawable.ic_menu_info_details);
        }

		return convertView;
	}
}
