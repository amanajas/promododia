package tarec.br.promododia.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import tarec.br.promododia.R;
import tarec.br.promododia.adapters.ByDayAdapter;
import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.dao.PromoDatabase;
import tarec.br.promododia.interfaces.OnFragmentInteractionListener;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Util;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link tarec.br.promododia.interfaces.OnFragmentInteractionListener}
 * interface.
 */
public class ByDayFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private OnFragmentInteractionListener mListener;

    public static Fragment newInstance() {
        ByDayFragment fragment = new ByDayFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ByDayFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promotions, container, false);

        double distance = PromoPreferences.getInstance(this.getActivity()).getDistanceLimit();
        final PromoList promotions = PromoDatabase.getInstance(this.getActivity()).getSearchByDistance(distance);

        mAdapter = new ByDayAdapter(this.getActivity(), promotions);

        // Set the adapter
        mListView = (ListView) view.findViewById(android.R.id.list);

        // Set adapter
        mListView.setAdapter(mAdapter);

        //Show empty list
        showEmptyList(view, promotions.getAmount() == 0);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction(view);

        }
    }
}
