package tarec.br.promododia.adapters;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import tarec.br.promododia.R;
import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Util;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BasePromoAdapter extends BaseAdapter {

    private WeakReference<Context> mContext;
    protected PromoList mPromoList;
    protected HashMap<String, PromoList> mListContent;
    protected List<String> mListHeader;
    protected double mDistanceLimit;

    private static final long START_ANIMATION_TIME = 30;
    private int mLastPosition;

    public BasePromoAdapter(final Context context, final PromoList list) {

		mContext = new WeakReference<Context>(context);
		mPromoList = list;
        mListContent = new HashMap<String, PromoList>();
        mListHeader = new ArrayList<String>();
        mLastPosition = -1;
        mDistanceLimit = 0;

        createContent();
	}

    protected String getHeaderGroupPromoAmount(final String headerTitle) {
        return " (" + getListPromo(headerTitle).getAmount() + ")";
    }

    protected void createContent() {

        // MUST DO SOMETHING IN THE CHILDREN
	}

    protected final PromoList getListPromo(final String group) {
        return mListContent.get(group);
    }

    protected final PromoList getListPromo(final int groupPosition) {
        return mListContent.get(mListHeader.get(groupPosition));
    }

	@Override
	public Object getItem(int position) {
        // MUST DO SOMETHING IN THE CHILDREN
		return null;
	}

    @Override
    public long getItemId(int position) {
        // MUST DO SOMETHING IN THE CHILDREN
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    protected final Context getContext() {
        return mContext.get();
    }

    @Override
	public int getCount() {
        // MUST DO SOMETHING IN THE CHILDREN
		return 0;
	}

    public static class PromoViewHolder {

        public enum HolderType {LIST, PROMO, NONE};

        public TextView title = null;
        public TextView desc = null;
        public ImageView star = null;
        public Promo promotion = null;
        public PromoList list = null;
        public HolderType type = HolderType.NONE;
    }

}
