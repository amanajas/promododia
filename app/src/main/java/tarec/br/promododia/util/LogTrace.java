package tarec.br.promododia.util;

import android.util.Log;

public class LogTrace {

	private static final String TAG = "Promo do Dia";

	public static void error(String text) {
        if (text != null) {
            //Log.e(TAG, text);
        }
	}
	
	public static void error(Exception e) {
        if (e != null)
		    error(e.getMessage());
	}
	
	
	public static void debug(String text) {
        if (text != null) {
            //Log.d(TAG, text);
        }
	}
	
	public static void debug(Exception e) {
        if (e != null)
            debug(e.getMessage());
	}
}
