package tarec.br.promododia.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import tarec.br.promododia.R;
import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.interfaces.OnFragmentInteractionListener;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.interfaces.IPromoRequestCallback;
import tarec.br.promododia.request.PromoRequest;
import tarec.br.promododia.util.LogTrace;
import tarec.br.promododia.util.Util;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link SetupFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SetupFragment extends Fragment implements SeekBar.OnSeekBarChangeListener, View.OnClickListener, IPromoRequestCallback {

    private OnFragmentInteractionListener mListener;

    private TextView mLastUpdateView;
    private TextView mMaxDistanceView;

    private int mMaxDistance;
    private int mMinDistance;
    private String mDefaultKm;
    private AlertDialog mProgressDialog;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment SetupFragment.
     */
    public static SetupFragment newInstance() {
        SetupFragment fragment = new SetupFragment();
        return fragment;
    }

    public SetupFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.setup_and_status, container, false);

        mLastUpdateView = (TextView) layout.findViewById(R.id.promotions_next_update);
        mMaxDistanceView = (TextView) layout.findViewById(R.id.setup_max_distance_display);
        SeekBar distanceBar = (SeekBar) layout.findViewById(R.id.distance_bar);
        Button btnUpdate = (Button) layout.findViewById(R.id.setup_btn_update_promos);

        showNextUpdate();

        distanceBar.setOnSeekBarChangeListener(this);
        btnUpdate.setOnClickListener(this);

        mMaxDistance = this.getActivity().getResources().getInteger(R.integer.default_promo_max_distance);
        mMinDistance = this.getActivity().getResources().getInteger(R.integer.default_promo_min_distance);
        mDefaultKm = this.getActivity().getResources().getString(R.string.distance_desc);

        distanceBar.setProgress(PromoPreferences.getInstance(this.getActivity()).getDistanceProgressBar());

        return layout;
    }

    private void showNextUpdate() {

        Date date = PromoPreferences.getInstance(this.getActivity()).getUpdateAccess();
        if (date != null) {
            mLastUpdateView.setText(Util.getDateToString(this.getActivity(), date));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        final int distance = (progress * (mMaxDistance - mMinDistance) / 100) + mMinDistance;
        mMaxDistanceView.setText(distance + mDefaultKm);

        PromoPreferences.getInstance(this.getActivity()).setDistanceLimit(distance, progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // DO NOTHING
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // DO NOTHING
    }

    @Override
    public void onClick(View v) {
        final AlertDialog.Builder alertBuilder = Util.getAlertBuilder(this.getActivity(),
                R.string.update_promo_question);
        alertBuilder.setPositiveButton(R.string.update_promo_question_choice_yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                final AlertDialog.Builder progressBuilder = Util.getAlertBuilder(SetupFragment.this.getActivity(),
                        R.string.update_promo_progress_message);
                if (Util.isNetworkAvailable(SetupFragment.this.getActivity())) {
                    final PromoRequest request = new PromoRequest(SetupFragment.this.getActivity());
                    request.forceExecution(SetupFragment.this);
                } else {
                    progressBuilder.setMessage(R.string.no_info_available);
                    progressBuilder.setNeutralButton(R.string.update_promo_question_choice_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }

                mProgressDialog = progressBuilder.create();
                mProgressDialog.show();

            }
        });
        alertBuilder.setNegativeButton(R.string.update_promo_question_choice_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = alertBuilder.create();
        dialog.show();
    }

    @Override
    public void onRequestFinished(PromoList list) {
        mProgressDialog.cancel();

        if (list == null) {

            AlertDialog.Builder builder = Util.getAlertBuilder(this.getActivity(), R.string.update_promo_error);
            builder.setNeutralButton(R.string.update_promo_question_choice_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {

            Toast.makeText(this.getActivity(), this.getString(R.string.update_promo_successful), Toast.LENGTH_LONG).show();
            showNextUpdate();
        }
    }

    @Override
    public void onStepInfo(String step) {
        LogTrace.debug(step);
    }

    @Override
    public void onException(Exception e) {
        mProgressDialog.cancel();
        LogTrace.error(e.toString());
    }
}
