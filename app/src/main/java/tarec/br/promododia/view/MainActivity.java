package tarec.br.promododia.view;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.SearchView;

import com.facebook.AppEventsLogger;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import tarec.br.promododia.R;
import tarec.br.promododia.adapters.BasePromoAdapter;
import tarec.br.promododia.config.PromoPreferences;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.interfaces.IAdViewListener;
import tarec.br.promododia.interfaces.INetworkBroadcast;
import tarec.br.promododia.interfaces.IPromoSearchCallback;
import tarec.br.promododia.interfaces.OnFragmentInteractionListener;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.request.PromoSearch;
import tarec.br.promododia.services.UpdateService;
import tarec.br.promododia.util.Constants;
import tarec.br.promododia.util.LogTrace;
import tarec.br.promododia.util.ToastAdListener;
import tarec.br.promododia.util.Util;

import com.facebook.Session;

public class MainActivity extends BaseActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,
        OnFragmentInteractionListener, IAdViewListener, INetworkBroadcast, IPromoSearchCallback {


    private static final int POSITION_SETUP_FRAGMENT = 4;
    private static final int POSITION_BY_CATEGORY_FRAGMENT = 3;
    private static final int POSITION_BY_DAY_FRAGMENT = 2;
    private static final int POSITION_ALL_PROMO_FRAGMENT = 1;

    private static final String STRING_NONE = "";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private CharSequence mTitle;
    private AdView mAdView;

    private IntentFilter mReceiverFilter;
    private BroadcastReceiver mNetworkReceiver;

    private String mSearchQuery;
    private AlertDialog mSearchDialog;

    private BroadcastReceiver mGPSReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.overridePendingTransition(R.anim.open_in, R.anim.open_out);

        mTitle = getTitle();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        mAdView = (AdView) findViewById(R.id.adView);
        mAdView.setAdListener(new ToastAdListener(this));
        mAdView.setVisibility(View.GONE);

        mReceiverFilter = new IntentFilter();
        mReceiverFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mNetworkReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

                    if (Util.isNetworkAvailable(context)) {
                        MainActivity.this.onNetworkConnected();
                    } else {
                        MainActivity.this.onNetworkDisconnected();
                    }
                }
            }
        };

        mGPSReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Constants.BROADCAST_GPS_UPDATED)) {

                    onNavigationDrawerItemSelected(0);
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(mGPSReceiver,
                new IntentFilter(Constants.BROADCAST_GPS_UPDATED));

        if (getIntent() != null) {
            mSearchQuery = getIntent().getStringExtra(SearchManager.QUERY);
            if (mSearchQuery != null && mSearchQuery.length() > 0) {

                getIntent().putExtra(SearchManager.QUERY, STRING_NONE);

                PromoSearch search = new PromoSearch(this, mSearchQuery);
                search.execute(this);

                AlertDialog.Builder dialog = Util.getAlertBuilder(this, R.string.search_promotions_msg);
                mSearchDialog = dialog.create();
                mSearchDialog.show();
            }
        }

        AppEventsLogger.activateApp(this);

        // Check if the service is up
        boolean isRunning = Util.checkService(this);
        if (!isRunning) {
            Intent startServiceIntent = new Intent(this, UpdateService.class);
            startService(startServiceIntent);
        }

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        Fragment fragScreen = null;

        onSectionAttached(position);

        switch (position) {

            case POSITION_ALL_PROMO_FRAGMENT:
                fragScreen = AllPromotionFragment.newInstance();
                break;

            case POSITION_BY_DAY_FRAGMENT:
                fragScreen = ByDayFragment.newInstance();
                break;

            case POSITION_BY_CATEGORY_FRAGMENT:
                fragScreen = ByCategoryFragment.newInstance();
                break;

            case POSITION_SETUP_FRAGMENT:
                fragScreen = SetupFragment.newInstance();
                break;

            default:
                fragScreen = DailyPromotionFragment.newInstance();
                break;
        }

        if (fragScreen != null) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragScreen)
                    .commit();
        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case POSITION_ALL_PROMO_FRAGMENT:
                mTitle = getString(R.string.title_section2);
                break;
            case POSITION_BY_DAY_FRAGMENT:
                mTitle = getString(R.string.title_section3);
                break;
            case POSITION_BY_CATEGORY_FRAGMENT:
                mTitle = getString(R.string.title_section4);
                break;
            case POSITION_SETUP_FRAGMENT:
                mTitle = getString(R.string.action_settings);
                break;
            default:
                mTitle = getString(R.string.app_name);
                break;
        }

        restoreActionBar();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    protected void onDestroy() {
        mAdView.destroy();
        PromoPreferences.getInstance(this).setLastLocation(0, 0);

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGPSReceiver);

        AppEventsLogger.deactivateApp(this);
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (isTaskRoot() && keyCode == KeyEvent.KEYCODE_BACK) {
            if (mNavigationDrawerFragment.isDrawerOpen()) {
                mNavigationDrawerFragment.closeMenu();
            } else {
                final FragmentManager fragmentManager = getSupportFragmentManager();
                int totalStack = fragmentManager.getBackStackEntryCount();
                if (totalStack == 0) {
                    showExitOption();
                    return true;
                } else if (totalStack > 0) {
                    fragmentManager.popBackStack();
                }
            }

            return false;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onFragmentInteraction(View view) {

        if (view.getTag() != null) {

            final FragmentManager fragmentManager = getSupportFragmentManager();
            final BasePromoAdapter.PromoViewHolder holder = (BasePromoAdapter.PromoViewHolder) view.getTag();

            if (holder.type == BasePromoAdapter.PromoViewHolder.HolderType.LIST) {

                PromoList list = holder.list;

                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.KEY_PROMO_LIST, list);
                bundle.putString(Constants.KEY_TITLE_LIST, holder.title.getText().toString());
                bundle.putString(Constants.KEY_DESC_LIST, holder.desc.getText().toString());

                Fragment fragScreen = ShowPromotionFragment.newInstance();
                fragScreen.setArguments(bundle);

                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragScreen)
                        .addToBackStack(null)
                        .commit();

            } else if (holder.type == BasePromoAdapter.PromoViewHolder.HolderType.PROMO) {

                Promo promo = holder.promotion;

                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.EXTRA_PROMO, promo);
                bundle.putString(Constants.KEY_TITLE_LIST, promo.getLabel());
                bundle.putString(Constants.KEY_DESC_LIST, Util.formatDistance(this, promo.getDistance()));

                Fragment fragScreen = DetailPromotionFragment.newInstance();
                fragScreen.setArguments(bundle);

                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragScreen)
                        .addToBackStack(null)
                        .commit();

            } else {

                fragmentManager.popBackStack();

            }
        }
    }

    @Override
    protected void onPause() {
        mAdView.pause();
        unregisterReceiver(mNetworkReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdView.resume();
        registerReceiver(mNetworkReceiver, mReceiverFilter);
    }

    @Override
    public void onLoaded() {
        LogTrace.debug("Ad loaded.");
        mAdView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClosed() {
        LogTrace.debug("Ad close.");
        mAdView.setVisibility(View.GONE);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onNetworkConnected() {
        LogTrace.debug("Network connected.");
        mAdView.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void onNetworkDisconnected() {

    }

    @Override
    public void onSearchFinished(PromoList list) {
        mSearchDialog.dismiss();

        if (list != null) {

            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.KEY_PROMO_LIST, list);
            bundle.putString(Constants.KEY_TITLE_LIST, getString(R.string.search_promotions_title));
            bundle.putString(Constants.KEY_DESC_LIST, String.valueOf(list.getAmount()));

            Fragment fragScreen = ShowPromotionFragment.newInstance();
            fragScreen.setArguments(bundle);

            final FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragScreen)
                    .addToBackStack(null)
                    .commit();

        }
    }

    @Override
    public void onSearchError(Exception e) {
        LogTrace.error(e);
        mSearchDialog.dismiss();
    }
}
