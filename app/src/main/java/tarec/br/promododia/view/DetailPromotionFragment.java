package tarec.br.promododia.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;

import org.w3c.dom.Text;

import tarec.br.promododia.R;
import tarec.br.promododia.adapters.BasePromoAdapter;
import tarec.br.promododia.adapters.ShowPromotionAdapter;
import tarec.br.promododia.dao.Promo;
import tarec.br.promododia.interfaces.OnFragmentInteractionListener;
import tarec.br.promododia.model.PromoList;
import tarec.br.promododia.util.Constants;
import tarec.br.promododia.util.Util;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link tarec.br.promododia.interfaces.OnFragmentInteractionListener}
 * interface.
 */
public class DetailPromotionFragment extends BaseFragment implements View.OnClickListener {

    private static final String SEARCH_GEO_LOCATION = "geo:0,0?q=";

    private OnFragmentInteractionListener mListener;

    private Promo mPromo;
    private String mTitle;
    private String mDescription;

    private UiLifecycleHelper mUiHelper;

    public static Fragment newInstance() {
        DetailPromotionFragment fragment = new DetailPromotionFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DetailPromotionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_promotions, container, false);

        if (savedInstanceState != null) {
            mPromo = (Promo) savedInstanceState.getSerializable(Constants.EXTRA_PROMO);
            mTitle = savedInstanceState.getString(Constants.KEY_TITLE_LIST, this.getActivity().
                    getResources().getString(R.string.none));
            mDescription = savedInstanceState.getString(Constants.KEY_DESC_LIST, this.getActivity().
                    getResources().getString(R.string.none));
        } else {
            mPromo = (Promo) getArguments().getSerializable(Constants.EXTRA_PROMO);
            mTitle = getArguments().getString(Constants.KEY_TITLE_LIST, this.getActivity().
                    getResources().getString(R.string.none));
            mDescription = getArguments().getString(Constants.KEY_DESC_LIST, this.getActivity().
                    getResources().getString(R.string.none));
        }

        mUiHelper = new UiLifecycleHelper(this.getActivity(), null);
        mUiHelper.onCreate(savedInstanceState);

        setBackButtonVisible(view, true);

        View back_button = view.findViewById(R.id.linear_view_click);
        back_button.setOnClickListener(this);

        TextView header = (TextView) back_button.findViewById(R.id.promo_header);
        TextView topDesc = (TextView) back_button.findViewById(R.id.promo_top_desc);

        header.setText(mTitle);
        topDesc.setText(mDescription);

        BasePromoAdapter.PromoViewHolder holder = new BasePromoAdapter.PromoViewHolder();
        holder.type = BasePromoAdapter.PromoViewHolder.HolderType.NONE;
        back_button.setTag(holder);

        TextView titleView = (TextView) view.findViewById(R.id.promo_detail_title);
        TextView dateView = (TextView) view.findViewById(R.id.promo_detail_end_date);
        TextView contentView = (TextView) view.findViewById(R.id.promo_detail_content);
        TextView periodicityView = (TextView) view.findViewById(R.id.promo_detail_periodicity);
        TextView addressView = (TextView) view.findViewById(R.id.promo_detail_address);
        Button showMap = (Button) view.findViewById(R.id.promo_detail_show_map);

        titleView.setText(mPromo.getTitle());
        dateView.setText(Util.getDateToString(this.getActivity(), mPromo.getEnd()));
        contentView.setText(mPromo.getDescription());
        periodicityView.setText(mPromo.getPeriodicity());
        addressView.setText(mPromo.getAddress());
        showMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Util.isNetworkAvailable(DetailPromotionFragment.this.getActivity())) {

                    AlertDialog.Builder builder = Util.getAlertBuilder(DetailPromotionFragment.this.getActivity(), R.string.show_map_question);

                    builder.setPositiveButton(R.string.show_map_question_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Uri uri = Uri.parse(SEARCH_GEO_LOCATION + mPromo.getAddress());
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                            DetailPromotionFragment.this.getActivity().startActivity(intent);

                        }
                    });

                    builder.setNegativeButton(R.string.show_map_question_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });

        Button share = (Button) view.findViewById(R.id.share_button);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Util.isNetworkAvailable(DetailPromotionFragment.this.getActivity())) {

                    AlertDialog.Builder builder = Util.getAlertBuilder(DetailPromotionFragment.this.getActivity(), R.string.share_promo_question);

                    builder.setPositiveButton(R.string.share_promo_question_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(DetailPromotionFragment.this.getActivity())
                                    .setLink(mPromo.getLink())
                                    .build();
                            mUiHelper.trackPendingDialogCall(shareDialog.present());

                        }
                    });

                    builder.setNegativeButton(R.string.share_promo_question_no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putSerializable(Constants.EXTRA_PROMO, mPromo);
        outState.putString(Constants.KEY_TITLE_LIST, mTitle);
        outState.putString(Constants.KEY_DESC_LIST, mDescription);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction(v);

        }
    }
}
